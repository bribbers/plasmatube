<!--
SPDX-FileCopyrightText: 2019 Linus Jahn <lnj@kaidan.im>

SPDX-License-Identifier: CC0-1.0
-->

# PlasmaTube

YouTube video player based on libmpv and youtube-dl.

